;;;; inspired.lisp

(in-package #:inspired)

(defparameter *feed-sanitized* (feedparser:sanitize-content
                                   (feedparser:parse-feed (drakma:http-request "https://tn.com.ar/feed/politica"))))
(defparameter *feed-wp-sanitized* (feedparser:sanitize-content
                                   (feedparser:parse-feed
                                    (drakma:http-request "http://feeds.washingtonpost.com/rss/politics"))))
(defparameter *feed-bbc-sanitized* (feedparser:sanitize-content
                                   (feedparser:parse-feed
                                    (drakma:http-request "http://feeds.bbci.co.uk/mundo/rss.xml"))))

(defparameter *current-x* 0
  "Current x coordinate")

(defparameter *current-y* 0
  "Current y coordinate")

(defparameter *height* 20
  "Height parameter for all svg images")

(defparameter *weigth* 20
  "Weight parameter for all svg images")

(defparameter *max-height* 300)
(defparameter *max-weight* 300)

(defparameter *scene* (svg:make-svg-toplevel 'svg:svg-1.1-toplevel :height *max-height* :width *max-weight*))

(defun pick-one-title (feed-sanitized)
  (let* ((entries (gethash :entries feed-sanitized))
         (first-entrie (first entries)))
    (split-sequence:split-sequence #\ (gethash :title first-entrie))))

(defun all-titles-feeds (feed-sanitized)
  (let* ((entries (gethash :entries feed-sanitized)))
    (mapcan (lambda (entrie)
              (split-sequence:split-sequence #\ (gethash :title entrie)))
            entries)))

(defun string->svg (string converter-function)
  "Converts string to svg"
  (funcall converter-function string))

(defun save-svg (scene &key (file #p"test.svg"))
  (with-open-file (s file :direction :output :if-exists :supersede)
    (svg:stream-out s scene)))

(defun gender-weigth-converter (string)
  (let* ((last-char (alexandria:last-elt string))
         (len-string (length string))
         (x (char-code (elt string 0)))
         (y (char-code last-char)))
    (cond ((find last-char "AaÁá")
           (svg:draw *scene* (:rect :x x :y y :height len-string
                                    :width len-string :fill "red")))
          ((find last-char "OoÓó")
           (svg:draw *scene* (:rect :x x :y y :height len-string
                                    :width len-string :fill "blue")))
          (t (svg:draw *scene* (:rect :x x :y y :height len-string
                                      :width len-string :fill "green"))))))

;TODO: create 1-1 converter function (use colors, waves, groups, etc)

(defun generate-svg (string-list f-converter file)
  (loop for string in string-list
     :do (string->svg string f-converter)
     :finally (save-svg *scene* :file file)))

(defun string<->svg (string &key (overlap nil))
  "1-1 string svg conversion"
  (loop for char across string :when (< *current-y* *max-height*)
     :do (char<->svg char :overlap overlap)))

(defun dup-str (str n)
  (let ((str-result ""))
    (dotimes (i n str-result)
      (setf str-result (concatenate 'string str str-result)))))

(defun int->rgb (int)
  "Convert an int into an RGB value in the format #FFFFFF"
  (let* ((int-mod (mod int (expt 16 6)))
         (str-hex (write-to-string int-mod :base 16))
         (len-str-hex (length str-hex))
         (zeros (- 6 len-str-hex)))
    (format nil "#~a~a" (dup-str "0" zeros) str-hex)))

(defun char<->svg (char &key (overlap nil))
  "1-1 char svg conversion, when overlap is nil ensures that every pixel is
  only given to one char representation"
  (if overlap
      (svg:draw *scene* (:rect :x 0 :y 0
                               :height 100
                               :width 100
                               :fill (int->rgb (char-code char))
                               ))
      (progn (svg:draw *scene* (:rect :x *current-x* :y *current-y*
                                          :height *height*
                                          :width *weigth*
                                          :fill (int->rgb (char-code char))
                                          ))
             (format t "~& ~a ~a ~%" *current-x* *current-y*)

             (cond ((< *current-x* *max-weight*) (incf *current-x* *weigth*))
                   ((>= *current-x* *max-weight*) (incf *current-y* *height*)
                    (setf *current-x* 0))))))

(defun init-params ()
  (defparameter *scene* (svg:make-svg-toplevel 'svg:svg-1.1-toplevel :height *max-height* :width *max-weight*))
  (defparameter *current-x* 0)
  (defparameter *current-y* 0))


(defmacro quick-svg (file &body body)
  `(progn (init-params)
          ,@body
          (if ,file
              (save-svg *scene* :file ,file))))

;; (setf path #P"/home/lisp/quicklisp/local-projects/inspired/test.svg")
;; (generate-svg (all-titles-feeds *feed-sanitized*) #'gender-weigth-converter path)
;; (generate-svg (pick-one-title *feed-sanitized*) #'gender-weigth-converter path)
;; (quick-svg nil (generate-svg (pick-one-title *feed-sanitized*) #'STRING<->SVG path))

;; (quick-svg path (svg:draw *scene* (:rect :x 0 :y 0
;;                                :height 100
;;                                :width 100
;;                                :fill (int->rgb (char-code (aref "&" 0)))
;;                                )))

;; (quick-svg path (STRING<->SVG "cristina"))
;; ver http://crscardellino.github.io/SBWCE/
