;; Module to define basic abstractions to convert text to other targets (currently images & sounds planned)


(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun gen-path-from-word (y string)
    (let* ((first-code-char (char-code (alexandria:first-elt string))))
      (loop
         for i from 1 to (1- (length string))
         for char across string
         :collect (let ((next-char (save-aref string i)))
                    (if next-char
                        (let ((code-char1 (char-code char))
                              (code-char2 (char-code next-char)))
                          `(svg:arc-to 30 30 0 0
                                       ,(get-direction code-char1 code-char2)
                                       ,code-char2 ,y))))))))

(defmacro gen-path-body (scene init-x init-y str)
  (let ((string (if (stringp str) str (symbol-value str))))
    `(svg:draw ,scene
               (:path :d (svg:path
                           (svg:move-to ,init-x ,init-y)
                           ,@(gen-path-from-word init-y string))
                      :fill "none" :stroke "blue" :stroke-width 1))))


(defun save-aref (array index)
  "Returns NIL if index is outside array limits"
  (let ((max-index (1- (length array))))
    (if (<= 0 index max-index)
        (aref array index))))

(defmethod chars-color (char1 char2)
  "Converts to chars into an RGB color"
  (let* ((ff (* 16 16))
         (code1 (char-code char1))
         (code2 (char-code char2))
         ;;for red component
         (r (write-to-string (mod code1 ff) :base 16))
         ;;for blue component
         (b (write-to-string (mod code2 ff) :base 16))
         ;;for green component
         (g (write-to-string (mod (+ code1 code2) (* 2 ff)) :base 16)))
    (format nil "~a~a~a" r g b)))


(defmethod get-ry (char1 char2)
  "Determines the length of the arc between to chars
  acording if thay were related previously"
  ;; TODO: calculate using relations attribute
  30)

(defmethod get-direction (char-code1 char-code2)
  (if (<= char-code1 char-code2) 1 0))

(defun augment-string (string &key (factor 2))
  "Multiplies char codes by 'factor' in order to augment the distance between
  char codes"
  (map 'string (lambda (char)
                 (code-char (* factor (char-code char))))
       string))

;; Testing
;; (defun save-svg (scene &key (file #p"test.svg"))
;;   (with-open-file (s file :direction :output :if-exists :supersede)
;;     (svg:stream-out s scene)))
;; (defparameter *max-height* 300)
;; (defparameter *max-weight* 300)
;; (defparameter *scene* (svg:make-svg-toplevel 'svg:svg-1.1-toplevel :height *max-height* :width *max-weight*))
;; (setf my-name (augment-string "alejandro" :factor 2))
;; (gen-path-body *scene* 200 200 my-name)
;; (gen-path-body *scene* 200 200 "alejandro")
;; (setf path #P"/home/lisp/quicklisp/local-projects/inspired/test2.svg")
;; (save-svg *scene* :file path)
