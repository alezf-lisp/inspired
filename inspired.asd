;;;; inspired.asd

(asdf:defsystem #:inspired
  :description "Converts texts to svg images"
  :author "Alejandro Zamora Fonseca <ale2014.zamora@gmail.com>"
  :license  "GPLv3"
  :version "0.0.1"
  :serial t
  :depends-on (:cl-svg :cl-feedparser :split-sequence :drakma :quri :str :graph)
  :components ((:file "package")
               (:file "inspired")))
